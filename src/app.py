"""Main Flask app entrypoint."""

import os

from flask import Flask
from flasgger import Swagger

from config import configs
from models import db
from routes.registry import registry

env = os.environ.get('FLASK_ENV', 'development')


def create_app(app_name='discovery-server'):
    """
    Function that returns a configured Flask app.
    """

    ROOT_PATH = os.path.dirname(__file__)
    app = Flask(app_name, root_path=ROOT_PATH)

    app.config['SWAGGER'] = {
        'title': 'DISCOVER-SERVER-API',
        'uiversion': 3,
    }
    Swagger(app)

    app.config.from_object(configs[env])

    app.register_blueprint(registry)

    db.init_app(app)
    with app.app_context():
        db.create_all()

    return app


if __name__ == '__main__':
    app = create_app()
    app.run()
