"""Virtual Object Blueprints."""

from flasgger import swag_from
from flask import Blueprint, request

from services.vo import add_vo, get_vos


registry = Blueprint('registry', __name__)


@registry.route('/project/<project>/vos', methods=['POST'])
@swag_from('swagger/create_vo.yaml')
def register_vo(project):
    """Registers a new Virtual Object."""

    request_data = request.get_json()
    cluster = request_data['cluster']
    catalogue_url = request_data['catalogue']
    thing_description = request_data['TD']

    add_vo(project, cluster, catalogue_url, thing_description)

    return 'VO successfully added.', 200


@registry.route('/project/<project>/vos', methods=['GET'])
@swag_from('swagger/get_vos.yaml')
def fetch_vos(project):
    """Fetches all Virtual Objects."""

    vos = get_vos(project)

    return vos, 200