"""VO table."""

from models import db
from sqlalchemy.dialects.postgresql import JSONB


class VO(db.Model):
    __tablename__ = 'vo'

    id = db.Column(db.Integer, primary_key=True)
    project = db.Column(db.String(255), nullable=False)
    name = db.Column(db.String(255))
    cluster = db.Column(db.String(255))
    catalogue_url = db.Column(db.String(255))
    thing_description = db.Column(JSONB)

    def to_dict(self):
        """Returns a dictionary representation of the class."""

        instance_dict = {
            'project': self.project,
            'name': self.name,
            'cluster': self.cluster,
            'catalogue_url': self.catalogue_url,
            'thing_description': self.thing_description
        }

        return instance_dict
