"""DB models declaration."""

from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()

from models.vo import VO
