"""Virtual Object services."""

from werkzeug.exceptions import BadRequest, NotFound

from models import db, VO


def add_vo(project, cluster, catalogue_url, thing_description):
    """Adds a Virtual Object to the database."""

    name = thing_description['title']

    vo = db.session.query(VO).filter_by(
        name=name,
        project=project).first()
    if vo is not None:
        raise BadRequest(f'VO with name {name} in project {project} already exists')

    vo = VO(
        project=project,
        name=name,
        cluster=cluster,
        catalogue_url=catalogue_url,
        thing_description=thing_description
    )

    db.session.add(vo)
    db.session.commit()

def get_vos(project):
    """Fetches VO of a certain project."""

    vos = db.session.query(VO).filter_by(project=project).all()
    project_vos = [vo.to_dict() for vo in vos]
    if not vos:
        raise NotFound('No VOs found.')

    return project_vos

