# VO-Discovery-Server

This repo hosts the Virtual Object Discovery Server that acts a directory of VOs where each VO registers itself. An interested user can query the directory and request a VO's catalogue URL to consume it.